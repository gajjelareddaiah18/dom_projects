const lights = document.querySelectorAll('.light');
let index = 0;

function changeColor() {
    lights.forEach(light => {
        light.classList.remove('green', 'red', 'yellow');

      
    });
    const color = ['green', 'red', 'yellow'][index];
    lights[index].classList.add(color);
}

setInterval(() => {
    changeColor();
    index = (index + 1) % lights.length;
    console.log(index);
}, 2000);
